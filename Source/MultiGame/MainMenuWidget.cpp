// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuWidget.h"
#include "Components/WidgetSwitcher.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"

#include "UObject/ConstructorHelpers.h"
#include "MultiGameInstance.h"


UMainMenuWidget::UMainMenuWidget(const FObjectInitializer & ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> ServerRawWBP(TEXT("/Game/MenuSystem/WBP_ServerRowWidget"));

	if (ServerRawWBP.Succeeded())
	{
		ServerRowClass = ServerRawWBP.Class;
	}
}

bool UMainMenuWidget::Initialize()
{
	Super::Initialize();

	Host->OnClicked.AddDynamic(this, &UMainMenuWidget::HostServer);
	HostSW->OnClicked.AddDynamic(this, &UMainMenuWidget::OpenHostMenu);
	Join->OnClicked.AddDynamic(this, &UMainMenuWidget::JoinServer);
	JoinSW->OnClicked.AddDynamic(this, &UMainMenuWidget::OpenJoinMenu);
	CancelJoin->OnClicked.AddDynamic(this, &UMainMenuWidget::BacktoMainMenu);
	CancelHost->OnClicked.AddDynamic(this, &UMainMenuWidget::BacktoMainMenu);
	//ServerList2->OnItemClicked().AddUObject(this,&UMainMenuWidget::Onitemclick);

	

	
	return true;
}

void UMainMenuWidget::JoinServer()
{
	if (Menuinterface != nullptr)
	Menuinterface->Join(SelectedIndex);
}

void UMainMenuWidget::HostServer()
{
	
	if (Menuinterface != nullptr) 
	{

		if (!ServerNameEntry->Text.IsEmpty())
		Menuinterface->Host(ServerNameEntry->GetText().ToString());
	}
}

void UMainMenuWidget::OpenJoinMenu()
{
	ServerList->ClearChildren();
	//ServerList2->ClearListItems();
	Menuinterface->RefreshServerList();
	MenuSwitcher->SetActiveWidget(JoinCanvas);

	
}

void UMainMenuWidget::OpenHostMenu()
{
	MenuSwitcher->SetActiveWidget(HostCanvas);
}

void UMainMenuWidget::BacktoMainMenu()
{
	MenuSwitcher->SetActiveWidget(MainCanvas);

	
}

void UMainMenuWidget::SetSelectedIndex(int32 index)
{
	SelectedIndex = index;
	int32 n= ServerList->GetChildrenCount();
	for (int i = 0; i < n; i++)
	{
		if(i!=index)
		{
			auto item=ServerList->GetChildAt(i);
			Cast<UServerRowWidget>(item)->ClearAll();
		}
	}
	
	
}

void UMainMenuWidget::Onitemclick(UObject *item)
{
	//auto ch=Cast<UButton>(item)->GetChildAt(0);

	UE_LOG(LogTemp, Warning, TEXT("selected is %s"),*item->GetName());

}

void UMainMenuWidget::SetServerList(TArray<FServerData> Servers)
{
	
	int32 i = 0;
	for(auto &server:Servers)
	{
		
		UServerRowWidget *ServerRaw = CreateWidget<UServerRowWidget>(this->GetGameInstance(), ServerRowClass);
		
		ServerRaw->Setup(this, i);
		

		ServerRaw->ServerName->SetText(FText::FromString(server.ServerName));
		ServerRaw->HostName->SetText(FText::FromString(server.HostName));

		FString SF = FString::Printf(TEXT("%d / %d"), server.CurrentPlayers, server.MaxPlayers);
		ServerRaw->ServerFraction->SetText(FText::FromString(SF));
		
		ServerList->AddChild(ServerRaw);
		
		//ServerList2->AddItem(ServerRaw);
	//	ServerRaw->OnListItemObjectSet(ServerRaw);
	
		++i;
		
	}
}

void UMainMenuWidget::OnLevelRemovedFromWorld(ULevel * InLevel, UWorld * InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
	
	//this->RemoveFromViewport();

	auto world = GetWorld();
	if (world != nullptr)
	{
		auto playercontroller = world->GetFirstPlayerController();
		FInputModeGameOnly inputmodedta;

		


		playercontroller->SetInputMode(inputmodedta);
	//	playercontroller->bShowMouseCursor = true;

	}

}


