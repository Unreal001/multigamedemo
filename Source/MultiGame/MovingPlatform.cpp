// Fill out your copyright notice in the Description page of Project Settings.

#include "MovingPlatform.h"

AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
	SetReplicates(true);
	SetReplicateMovement(true);
	}
	GlobalActorSart = GetActorLocation();
	GlobalTargetLocation =GetTransform().TransformPosition(TargetLocation);
}

void AMovingPlatform::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (HasAuthority())
	{
		auto Location = GetActorLocation();
		auto Travelled =Location - GlobalActorSart;
		auto ShouldTravelled = GlobalTargetLocation - GlobalActorSart;
		if (Travelled.Size() >= ShouldTravelled.Size())
		{
			auto swap = GlobalTargetLocation;
			 GlobalTargetLocation=GlobalActorSart;
			 GlobalActorSart = swap;


		}
		
			Location += (GlobalTargetLocation - GlobalActorSart).GetSafeNormal()*DeltaSeconds *Speed;
			SetActorLocation(Location);
		
	}
	
}