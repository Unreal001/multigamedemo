// Fill out your copyright notice in the Description page of Project Settings.

#include "InGameMenu.h"
#include "Components/Button.h"
#include "Kismet/KismetSystemLibrary.h"
bool UInGameMenu::Initialize()
{
	Super::Initialize();
	Quit->OnClicked.AddDynamic(this, &UInGameMenu::QuitF);
	Cancel->OnClicked.AddDynamic(this, &UInGameMenu::CancelF);
	
	return false;
}

void UInGameMenu::OnLevelRemovedFromWorld(ULevel * InLevel, UWorld * InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	//this->RemoveFromViewport();

	auto world = GetWorld();
	if (world != nullptr)
	{
		auto playercontroller = world->GetFirstPlayerController();
		FInputModeGameOnly inputmodedta;




		playercontroller->SetInputMode(inputmodedta);
	}
}

void UInGameMenu::QuitF()
{
	UE_LOG(LogTemp, Warning, TEXT("Quit"));
	auto world = GetWorld();

//	UKismetSystemLibrary::QuitGame(world, world->GetFirstPlayerController(), EQuitPreference::Quit);

	auto playercontroller = GetWorld()->GetFirstPlayerController();
	playercontroller->ClientTravel(FString("/Game/MainMenu"), ETravelType::TRAVEL_Absolute);
	
}

void UInGameMenu::CancelF()
{
	this->RemoveFromViewport();

	
	this->OnLevelRemovedFromWorld(GetWorld()->GetCurrentLevel(), GetWorld());

	
}




