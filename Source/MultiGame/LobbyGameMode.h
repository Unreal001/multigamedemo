// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MultiGameGameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API ALobbyGameMode : public AMultiGameGameMode
{
	GENERATED_BODY()

public:
		ALobbyGameMode();

		void PostLogin(APlayerController * NewPlayer)override;

		void GameStart();

	
	    void Logout(AController * Exiting)override;
private:
	uint32 count = 0;
	
};