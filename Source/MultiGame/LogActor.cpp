// Fill out your copyright notice in the Description page of Project Settings.

#include "LogActor.h"

#include "Core/PlayFabClientAPI.h"
using namespace PlayFab;
using namespace PlayFab::ClientModels;

// Sets default values
ALogActor::ALogActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ALogActor::OnLoginSucssess(const FLoginResult & Result) const
{

	UE_LOG(LogTemp, Log, TEXT("LoginSuccses"));
	auto player=GetWorld()->GetFirstPlayerController();
	

}

void ALogActor::OnRegistSuccsess(const PlayFab::ClientModels::FRegisterPlayFabUserResult & Result) const
{
	UE_LOG(LogTemp, Log, TEXT(" Reg Succses"));
}

void ALogActor::OnloginFailure(const FPlayFabCppError & Error) const
{
	UE_LOG(LogTemp, Log, TEXT("LoginFaild %s"),*FString(Error.ErrorMessage));
}

// Called when the game starts or when spawned
void ALogActor::BeginPlay()
{
	Super::BeginPlay();
	clientAPI = IPlayFabModuleInterface::Get().GetClientAPI();
	
	clientAPI->SetTitleId(TEXT("56C7E"));
	
	
	
	

}

// Called every frame
void ALogActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALogActor::Login(FString UserName, FString Password)
{
	FLoginWithPlayFabRequest req;
	req.Username = UserName;
	req.Password = Password;

	clientAPI->LoginWithPlayFab(req,
		PlayFab::UPlayFabClientAPI::FLoginWithCustomIDDelegate::CreateUObject(this, &ALogActor::OnLoginSucssess),

		PlayFab::FPlayFabErrorDelegate::CreateUObject(this, &ALogActor::OnloginFailure));
		
}

void ALogActor::Regist(FString UserName, FString Password)
{
	FRegisterPlayFabUserRequest req;
	req.Username = UserName;
	req.Password = Password;
	req.RequireBothUsernameAndEmail = false;
	clientAPI->RegisterPlayFabUser(req,UPlayFabClientAPI::FRegisterPlayFabUserDelegate::CreateUObject(this,&ALogActor::OnRegistSuccsess),
		PlayFab::FPlayFabErrorDelegate::CreateUObject(this, &ALogActor::OnloginFailure));
		
	//	)


}



