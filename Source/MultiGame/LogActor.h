// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PlayFab.h"
#include "Core/PlayFabError.h"
#include "Core/PlayFabClientDataModels.h"
#include "UILogInterface.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LogActor.generated.h"


UCLASS()
class MULTIGAME_API ALogActor : public AActor,public IUILogInterface
{
	GENERATED_BODY()


public:	
	// Sets default values for this actor's properties
	ALogActor();
	void OnLoginSucssess(const PlayFab::ClientModels::FLoginResult &Result) const;
	void OnRegistSuccsess(const PlayFab::ClientModels::FRegisterPlayFabUserResult &Result) const;
	void OnloginFailure(const PlayFab::FPlayFabCppError & Error) const;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Exec,BlueprintCallable)
	virtual void Login(FString UserName, FString Password);
	UFUNCTION(Exec, BlueprintCallable)
	virtual void Regist(FString UserName, FString Password);
private:
	PlayFabClientPtr clientAPI = nullptr;
	
};
