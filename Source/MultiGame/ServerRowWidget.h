// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include"Runtime/UMG/Public/Blueprint/IUserObjectListEntry.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Components/Border.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ServerRowWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API UServerRowWidget : public UUserWidget
{

	
	GENERATED_BODY()

	
		
public:

		UPROPERTY(meta = (BindWidget))
		UTextBlock *ServerName;

		UPROPERTY(meta = (BindWidget))
			UTextBlock *HostName;
	
		UPROPERTY(meta = (BindWidget))
			UTextBlock *ServerFraction;
		
		UPROPERTY(meta = (BindWidget))
			class UButton *ServerButton;
		
		
		UFUNCTION(BlueprintCallable)
			void OnServerClicked();

		UPROPERTY(BlueprintReadWrite)
			FLinearColor BorderColor=FLinearColor(ForceInit);
		
		UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly)
		void ClearAll() ;

	void Setup(class UMainMenuWidget *parent, int32 Serverindex);


	
// 		void OnListItemObjectSet(UObject * ListItemObject);
// 		
// 		virtual void	SetListItemObjectInternal(UObject* InObject)override;


private:
	int32 ServerIndex=-1;


	class UMainMenuWidget* parent;
};
