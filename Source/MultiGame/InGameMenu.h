// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MenuWidgetBase.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InGameMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API UInGameMenu : public UMenuWidgetBase
{
	GENERATED_BODY()


		virtual bool Initialize()override;
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)override;

	UPROPERTY(meta = (BindWidget))
		class	UButton *Cancel;


	UPROPERTY(meta = (BindWidget))
       class	UButton *Quit;

	UFUNCTION()
	void QuitF();
	
	UFUNCTION()
	void CancelF();
	
};
