// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidgetBase.h"

void UMenuWidgetBase::Setup() 
{
	this->AddToViewport();

	auto world = GetWorld();
	if (world != nullptr)
	{
		auto playercontroller = world->GetFirstPlayerController();
		FInputModeUIOnly inputmodedta;

		inputmodedta.SetWidgetToFocus(this->TakeWidget());
		inputmodedta.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);


		playercontroller->SetInputMode(inputmodedta);
		playercontroller->bShowMouseCursor = true;

	}


}
void UMenuWidgetBase::SetMenuInterface(IMenuInterface * menuInterface)
{

	this->Menuinterface = menuInterface;
}
