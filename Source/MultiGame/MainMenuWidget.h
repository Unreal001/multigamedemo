// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include"ServerRowWidget.h"

#include "Components/ScrollBox.h"
#include "MenuWidgetBase.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"


USTRUCT()
struct FServerData 
{
	GENERATED_BODY();
	FString ServerName;
	int16 CurrentPlayers;
	int16 MaxPlayers;
	FString HostName;
};


/**
 * 
 */
UCLASS()
class MULTIGAME_API UMainMenuWidget : public UMenuWidgetBase
{


	UMainMenuWidget(const FObjectInitializer & ObjectInitializer);


	GENERATED_BODY()

		virtual bool Initialize()override;

		UPROPERTY(meta = (BindWidget))
		class	UButton *Host;

		UPROPERTY(meta = (BindWidget))
			class	UButton *HostSW;
	
	UPROPERTY(meta = (BindWidget))
		class	UButton *JoinSW;
	
	UPROPERTY(meta = (BindWidget))
		class	UButton *Join;

	UPROPERTY(meta = (BindWidget))
		class	UButton *CancelJoin;

	UPROPERTY(meta = (BindWidget))
		class	UButton *CancelHost;

	UPROPERTY(meta = (BindWidget))
		class	UWidgetSwitcher *MenuSwitcher;
 
	UPROPERTY(meta = (BindWidget))
		class	UWidget *JoinCanvas;

	UPROPERTY(meta = (BindWidget))
		class	UWidget *MainCanvas;

	UPROPERTY(meta = (BindWidget))
		class	UWidget *HostCanvas;

	UPROPERTY(meta = (BindWidget))
		UScrollBox  *ServerList;
	
	UPROPERTY(meta = (BindWidget))
		class UEditableTextBox  *ServerNameEntry;

// 	UPROPERTY(meta = (BindWidget))
// 		UListView *ServerList2;


	
	UFUNCTION()
		void JoinServer();
	
	UFUNCTION()
		void HostServer();

	UFUNCTION()
		void OpenJoinMenu();

	UFUNCTION()
		void OpenHostMenu();


	UFUNCTION()
		void BacktoMainMenu();
private:
	int32 SelectedIndex;
	
public:
	UPROPERTY(BlueprintReadWrite)
		FLinearColor BorderColor = FLinearColor(ForceInit);
	void SetSelectedIndex(int32 index);
	
	TSubclassOf<UUserWidget> ServerRowClass;
	void Onitemclick(UObject  *item);
public:
	void SetServerList(TArray<FServerData> ServerNames);
protected:
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)override;

	
};
