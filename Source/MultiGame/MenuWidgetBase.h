// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "MenuInterface.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API UMenuWidgetBase : public UUserWidget
{
	GENERATED_BODY()
	
public:



	void Setup();

	void SetMenuInterface(IMenuInterface * menuInterface);

protected:
	class IMenuInterface * Menuinterface;
	
	
};
