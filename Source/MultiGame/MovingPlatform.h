// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API AMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()

public:
AMovingPlatform();
void BeginPlay()override;
void virtual Tick(float DeltaSeconds) override;

UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
FVector TargetLocation;


UPROPERTY(EditAnywhere)
float Speed;
private:
	FVector GlobalActorSart;
	FVector GlobalTargetLocation;

};
