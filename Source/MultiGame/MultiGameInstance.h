// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "MenuInterface.h"
#include"ServerRowWidget.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"  
#include "OnlineSessionInterface.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MultiGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MULTIGAME_API UMultiGameInstance : public UGameInstance,public IMenuInterface
{
	GENERATED_BODY()

		UMultiGameInstance(const FObjectInitializer & ObjectIn);

	
public:
	virtual void Init();

	UFUNCTION(Exec)
		void Host(FString ServerNameEntry);
	
	void CreateSession();
	
	UFUNCTION(Exec)
	 void Join(int32 index);
	
	UFUNCTION(BlueprintCallable)
	void LoadMenuWidget();
	
	UFUNCTION(BlueprintCallable)
	void LoadInGameMenu();

	void SessionStart();

	TSubclassOf<UUserWidget> Menuclass;
	TSubclassOf<UUserWidget>InGameMenuclass;
	
virtual	 void RefreshServerList();
 
class UMainMenuWidget* MainMenu=nullptr;

IOnlineSubsystem *oss;
 IOnlineSessionPtr  SessionInterface;
 TSharedPtr< FOnlineSessionSearch> SessionSearch;
 FString ServerNameEntry;


	
 void OnSessionCompleteCreated(FName SessionName, bool Successeded);
	
 void OnSessionCompleteDestroyed(FName SessionName, bool Successeded);
 void OnFindSessionsComplete( bool Successeded);

 void OnJoinSessionComplete(FName Sesseionname, EOnJoinSessionCompleteResult::Type  Result);
};
