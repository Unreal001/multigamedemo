// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiGameInstance.h"
#include"Engine/Engine.h"

#include "UObject/ConstructorHelpers.h"
#include "PlatFormTrigger.h"
#include"MainMenuWidget.h"
#include "InGameMenu.h"
#include "PlayFab.h"

const static FName Session_Name = NAME_GameSession;

FString ServerNameEntry;
UMultiGameInstance::UMultiGameInstance(const FObjectInitializer & ObjectIn)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuWidgetBP(TEXT("/Game/MenuSystem/WBP_MainMenu"));
	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuWidgetBP(TEXT("/Game/MenuSystem/WBP_InGameMenu"));
	
	if (ensure(MenuWidgetBP.Class)) 
	{
		Menuclass = MenuWidgetBP.Class;
		
	}
	if (ensure(InGameMenuWidgetBP.Class))
	{
		InGameMenuclass = InGameMenuWidgetBP.Class;

	}
	
	

}



void UMultiGameInstance::Init()
{
	Super::Init();

 	oss = IOnlineSubsystem::Get();


	if (oss == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("NUll OSS"));
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("OSS=%s"), *oss->GetSubsystemName().ToString());
	}
	SessionInterface = oss->GetSessionInterface();
	  
	SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnSessionCompleteCreated);
	SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnSessionCompleteDestroyed);
	SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnFindSessionsComplete);
	SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this,&UMultiGameInstance::OnJoinSessionComplete);
	
}

void UMultiGameInstance::Host(FString ServerNameEntry)
{
	this->ServerNameEntry = ServerNameEntry;
	auto ExistSession= SessionInterface->GetNamedSession(Session_Name);
	if(ExistSession)
	{
		SessionInterface->DestroySession(Session_Name);
		
	}
	else
	
	{
		
		CreateSession();
	}
}



void UMultiGameInstance::Join(int32 index)
{
// 	UEngine* Engine = GetEngine();
// 	Engine->AddOnScreenDebugMessage(0, 5, FColor::Blue,FString::Printf(TEXT("%s Joind"),*Address));
// 	
	//auto playercontroller = GetFirstLocalPlayerController();
	//playercontroller->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
	if(oss)
	{
		if (SessionSearch->SearchState== EOnlineAsyncTaskState::Done)
		{
			if(index!=-1)
			{
				SessionInterface->JoinSession(0,Session_Name,SessionSearch->SearchResults[index]);
			}
		}
	}
	
}

void UMultiGameInstance::LoadMenuWidget()
{
	MainMenu=CreateWidget<UMainMenuWidget>(this, Menuclass);
	MainMenu->Setup();

	MainMenu->SetMenuInterface(this);
	
}

void UMultiGameInstance::LoadInGameMenu()
{
	auto ingamemenu = CreateWidget<UInGameMenu>(this, InGameMenuclass);
	
	ingamemenu->Setup();
	
	ingamemenu->SetMenuInterface(this);
}

void UMultiGameInstance::SessionStart()
{
	SessionInterface->StartSession(Session_Name);
}

void UMultiGameInstance::RefreshServerList()
{
	SessionSearch = MakeShareable(new FOnlineSessionSearch);
	//SessionSearch->bIsLanQuery = true;
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE,true,EOnlineComparisonOp::Equals);
	SessionSearch->MaxSearchResults = 100;
	if (SessionSearch.IsValid())
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}

void UMultiGameInstance::OnSessionCompleteCreated(FName SessionName, bool Successeded)
{
	if(Successeded)
	{
		auto world = GetWorld();
		world->ServerTravel(FString("/Game/Maps/Lobby?listen"));
	}
}

void UMultiGameInstance::OnSessionCompleteDestroyed(FName SessionName, bool Successeded)
{
	if (Successeded)
	{
		CreateSession();
	}
}

void UMultiGameInstance::OnFindSessionsComplete(bool Successeded)
{
	if(Successeded)
	{
	auto Results = SessionSearch->SearchResults;

	UE_LOG(LogTemp, Warning, TEXT("Sessions=%d"), Results.Max());
	TArray<FServerData> ServerNames;

	for (FOnlineSessionSearchResult &Res : SessionSearch->SearchResults)
	{
		FServerData data;
		  Res.Session.SessionSettings.Get(FName("ServerName"), data.ServerName);
	
		data.HostName = Res.Session.OwningUserName;
		data.MaxPlayers = Res.Session.SessionSettings.NumPublicConnections;
		data.CurrentPlayers =  data.MaxPlayers-  Res.Session.NumOpenPublicConnections;
		ServerNames.Add(data);
		UE_LOG(LogTemp, Warning, TEXT("Session is %s"), *Res.GetSessionIdStr());
	}
	MainMenu->SetServerList(ServerNames);
     }
}
void UMultiGameInstance::OnJoinSessionComplete(FName Sesseionname, EOnJoinSessionCompleteResult::Type Result)
{
	if (Result== EOnJoinSessionCompleteResult::Success)
	{
		FString Address;
		SessionInterface->GetResolvedConnectString(Sesseionname, Address);
		UE_LOG(LogTemp, Warning, TEXT("Session=======%s"),*Address);
		auto playercontroller = GetFirstLocalPlayerController();
		playercontroller->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
	}
}


void UMultiGameInstance::CreateSession()
{
	if (SessionInterface.IsValid())
	{
		
		FOnlineSessionSettings SessionSettings;

		if (oss->GetSubsystemName().ToString() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		else 
		{
			SessionSettings.bIsLANMatch = false;
		}
		
		SessionSettings.NumPublicConnections = 4;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;
		SessionSettings.Set(FName("ServerName"), ServerNameEntry, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
		SessionInterface->CreateSession(0, Session_Name, SessionSettings);
	}
}
