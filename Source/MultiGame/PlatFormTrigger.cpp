// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatFormTrigger.h"


// Sets default values
APlatFormTrigger::APlatFormTrigger()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	PrimaryActorTick.bCanEverTick = true;

	PressureVolume = CreateDefaultSubobject<UBoxComponent>(FName("PressureVolme"));

	RootComponent = PressureVolume;
	if (PressureVolume->GetGenerateOverlapEvents())
	{
		PressureVolume->OnComponentEndOverlap.AddDynamic(this, &APlatFormTrigger::OnOverlapEnd);
		;
	}


}

// Called when the game starts or when spawned
void APlatFormTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	PressureVolume->OnComponentBeginOverlap.AddDynamic(this, &APlatFormTrigger::OnOverlapStart);

}


// Called every frame
void APlatFormTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void APlatFormTrigger::OnOverlapStart(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("ACTIVATIED"));
}

void APlatFormTrigger::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("DECTIVATIED"));
}

