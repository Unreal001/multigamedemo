// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "MultiGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "MultiGameInstance.h"

ALobbyGameMode::ALobbyGameMode() 
{
	//AMultiGameGameMode();

	
}


void ALobbyGameMode::PostLogin(APlayerController * NewPlayer)
{
	Super::PostLogin(NewPlayer);
	count++;

	bUseSeamlessTravel = true;
	if(count>=2)
	{
		FTimerHandle MyTimerHandle;
		
		GetWorldTimerManager().SetTimer(MyTimerHandle, this, &ALobbyGameMode::GameStart, 5);

	
	}
}

void ALobbyGameMode::GameStart()
{
	Cast<UMultiGameInstance>(GetGameInstance())->SessionStart();
	auto world = GetWorld();
	world->ServerTravel(FString("/Game/Maps/ThirdPersonExampleMap?listen"));
}

void ALobbyGameMode::Logout(AController * Exiting)
{
	Super::Logout(Exiting);
	count--;
}
